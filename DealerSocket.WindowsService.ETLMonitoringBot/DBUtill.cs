﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealerSocket.WindowsService.ETLMonitoringBot
{
    class DBUntill
    {
        private string DbConnectionString;

        private string ETLStatusQuery =
            "SELECT top 1 ControlLogId, c.ControlLogStatusTypeId, ControlLogStatusTypeName, ControlLogStatusTypeDescription, c.UpdateDate, EndTime "
            + "FROM ControlLog AS c with(nolock) INNER JOIN ControlLogStatusType AS t with(nolock) "
            + "ON c.ControlLogStatusTypeId = t.ControlLogStatusTypeId ORDER BY ControlLogId DESC";

        //private string ETLCompletedStepSummary =
        //    "SELECT DATEDIFF(minute, d.StartTime, d.EndTime) AS RunTimeMinutes, pt.LoadProcessTypeDescription, "
        //    + " t.DetailStatusTypeDescription, d.EndTime, d.StartTime, d.ProcessDetailId FROM LoadProcessDetail AS d with(nolock) "
        //    + " INNER JOIN LoadProcessHeader AS h with(nolock) ON d.ProcessHeaderId = h.ProcessHeaderId "
        //    + " INNER JOIN DetailStatusType AS t with(nolock) ON d.DetailStatusTypeId = t.DetailStatusTypeId"
        //    + " INNER JOIN LoadProcessType AS pt with(nolock) ON h.LoadProcessTypeId = pt.LoadProcessTypeId"
        //    + " WHERE d.ControlLogId = @ControlLogId AND d.DetailStatusTypeId  IN (3,4)"
        //    + " AND LoadProcessTypeDescription not like '%DEALER SITE TO STAGING DB%' order by EndTime desc";

        private string ETLCompletedStepSummary =
            "SELECT pt.LoadProcessTypeDescription, MAX(t.DetailStatusTypeDescription) AS DetailStatusTypeDescription, MAX(d.EndTime) AS MaxEndTime, MIN(d.StartTime) AS MaxStartTime,  "
            + " MAX(d.ProcessDetailId) AS ProcessDetailId, DATEDIFF(minute, MIN(d.StartTime), MAX(d.EndTime)) AS RunTimeMinutes "
            + " FROM LoadProcessDetail AS d with(nolock) "
            + " INNER JOIN LoadProcessHeader AS h with(nolock) ON d.ProcessHeaderId = h.ProcessHeaderId "
            + " INNER JOIN DetailStatusType AS t with(nolock) ON d.DetailStatusTypeId = t.DetailStatusTypeId"
            + " INNER JOIN LoadProcessType AS pt with(nolock) ON h.LoadProcessTypeId = pt.LoadProcessTypeId"
            + " WHERE d.ControlLogId = @ControlLogId AND d.DetailStatusTypeId  IN (3,4)"
            + " GROUP BY pt.LoadProcessTypeDescription ORDER BY MaxEndTime DESC";

        private string ETLTopLongRunningSteps =
            "SELECT TOP 5 DATEDIFF(minute, StartTime, EndTime) AS RunTimeMinutes, ProcessName, StartTime, "
            + "EndTime, RecordsInserted, RecordsUpdated, RecordsDeleted from EDRPerformanceStatistic with(nolock) "
            + "WHERE ProcessDetailId = @ProcessDetailId AND ProcessName LIKE '%.dtsx' AND ProcessName NOT LIKE 'FactTableLoad.dtsx' "
            + "Order BY RunTimeMinutes DESC ";

        private string ETLTotalRecordsProcessed =
            "SELECT (SUM(RecordsInserted) + SUM(RecordsUpdated) + SUM(RecordsDeleted)) AS TotalRecords "
            + "FROM EDRPerformanceStatistic WITH(NOLOCK) "
            + "WHERE ProcessDetailId = @ProcessDetailId ";

        private string ETLTopLongRunningStepsWithDataFlow =
            "SELECT TOP 5 DATEDIFF(minute, StartTime, EndTime) AS RunTimeMinutes, ProcessName, StartTime, "
            + "EndTime, RecordsInserted, RecordsUpdated, RecordsDeleted from EDRPerformanceStatistic with(nolock) "
            + "WHERE ProcessDetailId = @ProcessDetailId AND (RecordsInserted > 0 OR  RecordsUpdated> 0 OR RecordsDeleted > 0) "
            + "Order BY RunTimeMinutes DESC ";

        private string ETLTopLongRunningFactLoadProcess =
            "SELECT TOP 5 DATEDIFF(minute, StartTime, GETDATE()) AS RunTimeMinutes, ProcessName, StartTime "
            + "from EDRPerformanceStatistic with(nolock) "
            + "WHERE ProcessDetailId IN (SELECT TOP 1 ProcessDetailId FROM LoadProcessDetail with(nolock) "
            + "WHERE ControlLogId=@ControlLogId order by StartTime DESC) "
            + "AND EndTime IS NULL AND ProcessName LIKE '%.dtsx' AND ProcessName NOT LIKE 'FactTableLoad.dtsx' "
            + "AND DATEDIFF(minute, StartTime, GETDATE()) > 38 Order BY RunTimeMinutes DESC ";

        private string ETLStageDBCompleteCount =
            "SELECT COUNT(d.ProcessDetailId) AS DBCount FROM LoadProcessDetail AS d with(nolock) "
            + " INNER JOIN LoadProcessHeader AS h with(nolock) ON d.ProcessHeaderId = h.ProcessHeaderId "
            + " INNER JOIN LoadProcessType AS pt with(nolock) ON h.LoadProcessTypeId = pt.LoadProcessTypeId"
            + " WHERE d.ControlLogId = @ControlLogId AND d.DetailStatusTypeId  IN (3,4)"
            + " AND LoadProcessTypeDescription like '%DEALER SITE TO STAGING DB%'";


        private string ETLTopLongRunningDBStageTable =
            "SELECT TOP 5 DATEDIFF(minute, d.StartTime, d.EndTime) AS RunTimeMinutes, pt.LoadProcessTypeDescription,t.DetailStatusTypeDescription, "
            + "sd.DatabaseName,d.EndTime,d.StartTime,sInfo.ServerName FROM LoadProcessDetail AS d with(nolock) "
            + "INNER JOIN LoadProcessHeader AS h with(nolock) ON d.ProcessHeaderId = h.ProcessHeaderId "
            + "INNER JOIN DetailStatusType AS t with(nolock) ON d.DetailStatusTypeId = t.DetailStatusTypeId "
            + "INNER JOIN ServerDatabaseAssociation AS src with(nolock) ON h.SourceServerDBAssociationId = src.ServerDBAssociationId "
            + "INNER JOIN ServerDatabaseAssociation AS dest with(nolock) ON h.DestinationServerDBAssociationId = dest.ServerDBAssociationId "
            + "INNER JOIN DatabaseInformation AS sd with(nolock) ON src.DatabaseId = sd.DatabaseId "
            + "INNER JOIN LoadProcessType AS pt with(nolock) ON h.LoadProcessTypeId = pt.LoadProcessTypeId "
            + "INNER JOIN ServerInformation AS sInfo with(nolock) ON sInfo.serverId = src.serverId "
            + "WHERE d.ControlLogId = @ControlLogId AND DetailStatusTypeDescription  like '%COMPLETE%' "
            + "and LoadProcessTypeDescription like '%DEALER SITE TO STAGING DB%' ORDER BY RunTimeMinutes desc";

        private string ETLLongInProcessDBStageTable =
            "SELECT DATEDIFF(minute, d.StartTime, GETDATE()) AS RunTimeMinutes, " +
            "(CASE WHEN sd.IsEDRSubscriber =1 THEN 'Subscriber' ELSE 'Non-Subscriber' END) AS Sub_Status,"
            + "(CASE WHEN sd.IsRequiredDB =1 THEN 'Required' ELSE 'Not Required' END) AS IsRequired, "
            + "(CASE WHEN seq.ProcessName IS NULL THEN 'Sequence Not Started' ELSE seq.ProcessName END) AS SequenceNum,"
            + "sd.DatabaseName,d.StartTime,sInfo.ServerName FROM LoadProcessDetail AS d with(nolock) "
            + "INNER JOIN LoadProcessHeader AS h with(nolock) ON d.ProcessHeaderId = h.ProcessHeaderId "
            + "INNER JOIN ServerDatabaseAssociation AS src with(nolock) ON h.SourceServerDBAssociationId = src.ServerDBAssociationId "
            + "INNER JOIN DatabaseInformation AS sd with(nolock) ON src.DatabaseId = sd.DatabaseId "
            + "INNER JOIN LoadProcessType AS pt with(nolock) ON h.LoadProcessTypeId = pt.LoadProcessTypeId "
            + "INNER JOIN ServerInformation AS sInfo with(nolock) ON sInfo.serverId = src.serverId "
            + "OUTER APPLY (SELECT TOP 1 ProcessName  FROM dbo.EDRPerformanceStatistic AS ep WITH (NOLOCK)"
            + "               WHERE ProcessName LIKE 'Sequence-Load Staging Tables%'"
            + "               and ProcessDetailId = d.ProcessDetailId"
            + "               ORDER BY StartTime DESC"
            + "             ) AS seq "
            + "WHERE d.ControlLogId = @ControlLogId AND d.EndTime IS NULL AND DATEDIFF(minute, d.StartTime, GETDATE()) > 20 "
            + "and LoadProcessTypeDescription like '%DEALER SITE TO STAGING DB%' ORDER BY RunTimeMinutes desc";

        public DBUntill()
        {
            //this.DbConnectionString = "Data Source=SLCDW04.PROD.dealersocket.net;Initial Catalog=EnterpriseDataReportingAdministration;Integrated Security=True";
            this.DbConnectionString = ConfigurationManager.ConnectionStrings["ETLLogDB"].ConnectionString;
        }

        internal DataTable GetETLSTatus()
        {
            DataTable dtETLStatus = new DataTable();
            dtETLStatus.Columns.Add("ControlLogId");
            dtETLStatus.Columns.Add("ShortStatus");
            dtETLStatus.Columns.Add("LongStatus");
            dtETLStatus.Columns.Add("StartTime");
            dtETLStatus.Columns.Add("EndTime");
            dtETLStatus.Columns.Add("ControlLogStatusTypeId");
            dtETLStatus.TableName = "ETLStatus";
            try
            {
                using (var sqlConnection = new SqlConnection(this.DbConnectionString))
                {
                    using (var sqlCommand = new SqlCommand())
                    {
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText = ETLStatusQuery;

                        sqlConnection.Open();
                        SqlDataReader sqlDataReader;
                        using (sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            if (sqlDataReader.HasRows)
                            {
                                while (sqlDataReader.Read())
                                {
                                    try
                                    {
                                        DataRow tableRow = dtETLStatus.NewRow();
                                        tableRow["ControlLogId"] = sqlDataReader["ControlLogId"];
                                        tableRow["ShortStatus"] = sqlDataReader["ControlLogStatusTypeName"];
                                        tableRow["LongStatus"] = sqlDataReader["ControlLogStatusTypeDescription"];
                                        tableRow["StartTime"] = sqlDataReader["UpdateDate"];
                                        tableRow["EndTime"] = sqlDataReader["EndTime"];
                                        tableRow["ControlLogStatusTypeId"] = sqlDataReader["ControlLogStatusTypeId"];

                                        dtETLStatus.Rows.Add(tableRow);
                                    }
                                    catch (Exception e)
                                    {
                                        var logError = new ServiceInvoke();
                                        logError.POST(e);
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                var logError = new ServiceInvoke();
                logError.POST(e);
            }

            return dtETLStatus;
        }

        internal DataTable GetCompletedStepSummary(string controlLogId)
        {
            DataTable dtETLStatus = new DataTable();
            dtETLStatus.Columns.Add("TimeTaken");
            dtETLStatus.Columns.Add("StepDiscription");
            dtETLStatus.Columns.Add("StepSatus");
            dtETLStatus.Columns.Add("StartTime");
            dtETLStatus.Columns.Add("EndTime");
            dtETLStatus.Columns.Add("ProcessDetailId");
            dtETLStatus.TableName = "ETLStepSummary";
            try
            {
                using (var sqlConnection = new SqlConnection(this.DbConnectionString))
                {
                    using (var sqlCommand = new SqlCommand())
                    {
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText = ETLCompletedStepSummary;
                        sqlCommand.Parameters.Add(new SqlParameter("@ControlLogId", controlLogId));

                        sqlConnection.Open();
                        SqlDataReader sqlDataReader;
                        using (sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            if (sqlDataReader.HasRows)
                            {
                                while (sqlDataReader.Read())
                                {
                                    try
                                    {
                                        DataRow tableRow = dtETLStatus.NewRow();
                                        tableRow["TimeTaken"] = sqlDataReader["RunTimeMinutes"];
                                        tableRow["StepDiscription"] = sqlDataReader["LoadProcessTypeDescription"];
                                        tableRow["StepSatus"] = sqlDataReader["DetailStatusTypeDescription"];
                                        tableRow["StartTime"] = sqlDataReader["MaxStartTime"];
                                        tableRow["EndTime"] = sqlDataReader["MaxEndTime"];
                                        tableRow["ProcessDetailId"] = sqlDataReader["ProcessDetailId"];


                                        dtETLStatus.Rows.Add(tableRow);
                                    }
                                    catch (Exception e)
                                    {
                                        var logError = new ServiceInvoke();
                                        logError.POST(e);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var logError = new ServiceInvoke();
                logError.POST(e);
            }

            return dtETLStatus;
        }

        internal DataTable GetETLTopLongRunningSteps(string processDetailId, bool isDataFlowRequired = false)
        {
            DataTable dtETLData = new DataTable();
            dtETLData.Columns.Add("TimeTaken");
            dtETLData.Columns.Add("ProcessName");
            dtETLData.Columns.Add("StartTime");
            dtETLData.Columns.Add("EndTime");
            dtETLData.Columns.Add("RecordsInserted");
            dtETLData.Columns.Add("RecordsUpdated");
            dtETLData.Columns.Add("RecordsDeleted");
            dtETLData.TableName = "ETLTop3LongRunning";

            try
            {
                using (var sqlConnection = new SqlConnection(this.DbConnectionString))
                {
                    using (var sqlCommand = new SqlCommand())
                    {
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText = isDataFlowRequired ? ETLTopLongRunningStepsWithDataFlow : ETLTopLongRunningSteps;
                        sqlCommand.Parameters.Add(new SqlParameter("@ProcessDetailId", processDetailId));

                        sqlConnection.Open();
                        SqlDataReader sqlDataReader;
                        using (sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            if (sqlDataReader.HasRows)
                            {
                                while (sqlDataReader.Read())
                                {
                                    try
                                    {
                                        DataRow tableRow = dtETLData.NewRow();
                                        tableRow["TimeTaken"] = sqlDataReader["RunTimeMinutes"];
                                        tableRow["ProcessName"] = sqlDataReader["ProcessName"];
                                        tableRow["StartTime"] = sqlDataReader["StartTime"];
                                        tableRow["EndTime"] = sqlDataReader["EndTime"];
                                        tableRow["RecordsInserted"] = sqlDataReader["RecordsInserted"];
                                        tableRow["RecordsUpdated"] = sqlDataReader["RecordsUpdated"];
                                        tableRow["RecordsDeleted"] = sqlDataReader["RecordsDeleted"];

                                        dtETLData.Rows.Add(tableRow);
                                    }
                                    catch (Exception e)
                                    {
                                        var logError = new ServiceInvoke();
                                        logError.POST(e);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var logError = new ServiceInvoke();
                logError.POST(e);
            }

            return dtETLData;
        }

        internal Int64 GetETLTotalDataVolume(string processDetailId)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(this.DbConnectionString))
                {
                    using (var sqlCommand = new SqlCommand())
                    {
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText = ETLTotalRecordsProcessed;
                        sqlCommand.Parameters.Add(new SqlParameter("@ProcessDetailId", processDetailId));

                        sqlConnection.Open();
                        SqlDataReader sqlDataReader;
                        using (sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            if (sqlDataReader.HasRows)
                            {
                                if (sqlDataReader.Read())
                                {
                                    try
                                    {
                                        return Convert.ToInt64(sqlDataReader["TotalRecords"]);
                                    }
                                    catch (Exception e)
                                    {
                                        var logError = new ServiceInvoke();
                                        logError.POST(e);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var logError = new ServiceInvoke();
                logError.POST(e);
            }

            return 0;
        }

        internal DataTable GetETLTopLongRunningFactProcess(string controlLogId)
        {
            DataTable dtETLData = new DataTable();
            dtETLData.Columns.Add("TimeTaken");
            dtETLData.Columns.Add("ProcessName");
            dtETLData.Columns.Add("StartTime");
            dtETLData.TableName = "ETLTop3LongRunningFact";

            try
            {
                using (var sqlConnection = new SqlConnection(this.DbConnectionString))
                {
                    using (var sqlCommand = new SqlCommand())
                    {
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText = ETLTopLongRunningFactLoadProcess;
                        sqlCommand.Parameters.Add(new SqlParameter("@ControlLogId", controlLogId));

                        sqlConnection.Open();
                        SqlDataReader sqlDataReader;
                        using (sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            if (sqlDataReader.HasRows)
                            {
                                while (sqlDataReader.Read())
                                {
                                    try
                                    {
                                        DataRow tableRow = dtETLData.NewRow();
                                        tableRow["TimeTaken"] = sqlDataReader["RunTimeMinutes"];
                                        tableRow["ProcessName"] = sqlDataReader["ProcessName"];
                                        tableRow["StartTime"] = sqlDataReader["StartTime"];

                                        dtETLData.Rows.Add(tableRow);
                                    }
                                    catch (Exception e)
                                    {
                                        var logError = new ServiceInvoke();
                                        logError.POST(e);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var logError = new ServiceInvoke();
                logError.POST(e);
            }

            return dtETLData;
        }

        internal int GetETLStageCompleteDBCount(string controlLogId)
        {
            int dbCountResult = 0;
            try
            {
                using (var sqlConnection = new SqlConnection(this.DbConnectionString))
                {
                    using (var sqlCommand = new SqlCommand())
                    {
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText = ETLStageDBCompleteCount;
                        sqlCommand.Parameters.Add(new SqlParameter("@ControlLogId", controlLogId));

                        sqlConnection.Open();
                        SqlDataReader sqlDataReader;
                        using (sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            if (sqlDataReader.HasRows)
                            {
                                if (sqlDataReader.Read())
                                {
                                    try
                                    {
                                        dbCountResult = Convert.ToInt32(sqlDataReader["DBCount"]);
                                    }
                                    catch (Exception e)
                                    {
                                        var logError = new ServiceInvoke();
                                        logError.POST(e);
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                var logError = new ServiceInvoke();
                logError.POST(e);
            }

            return dbCountResult;
        }

        internal DataTable GetTopLongRunningDBStageTable(string controlLogId)
        {
            DataTable dtETLData = new DataTable();
            dtETLData.Columns.Add("TimeTaken");
            dtETLData.Columns.Add("LongStatus");
            dtETLData.Columns.Add("StartTime");
            dtETLData.Columns.Add("EndTime");
            dtETLData.Columns.Add("DatabaseName");
            dtETLData.Columns.Add("ServerName");
            dtETLData.TableName = "ETLTop3LongRunningDBStage";

            try
            {
                using (var sqlConnection = new SqlConnection(this.DbConnectionString))
                {
                    using (var sqlCommand = new SqlCommand())
                    {
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText = ETLTopLongRunningDBStageTable;
                        sqlCommand.Parameters.Add(new SqlParameter("@ControlLogId", controlLogId));

                        sqlConnection.Open();
                        SqlDataReader sqlDataReader;
                        using (sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            if (sqlDataReader.HasRows)
                            {
                                while (sqlDataReader.Read())
                                {
                                    try
                                    {
                                        DataRow tableRow = dtETLData.NewRow();
                                        tableRow["TimeTaken"] = sqlDataReader["RunTimeMinutes"];
                                        tableRow["LongStatus"] = sqlDataReader["DetailStatusTypeDescription"];
                                        tableRow["StartTime"] = sqlDataReader["StartTime"];
                                        tableRow["EndTime"] = sqlDataReader["EndTime"];
                                        tableRow["DatabaseName"] = sqlDataReader["DatabaseName"];
                                        tableRow["ServerName"] = sqlDataReader["ServerName"];

                                        dtETLData.Rows.Add(tableRow);
                                    }
                                    catch (Exception e)
                                    {
                                        var logError = new ServiceInvoke();
                                        logError.POST(e);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
               var logError = new ServiceInvoke();
               logError.POST(e);
            }

            return dtETLData;
        }

        internal DataTable GetLongInProcessDBStageTable(string controlLogId)
        {
            DataTable dtETLData = new DataTable();
            dtETLData.Columns.Add("TimeTaken");
            dtETLData.Columns.Add("StartTime");
            dtETLData.Columns.Add("DatabaseName");
            dtETLData.Columns.Add("ServerName");
            dtETLData.Columns.Add("Subscription_Status");
            dtETLData.Columns.Add("IsRequired");
            dtETLData.Columns.Add("Sequence");
            dtETLData.TableName = "ETLLongInProcessDBStage";

            try
            {
                using (var sqlConnection = new SqlConnection(this.DbConnectionString))
                {
                    using (var sqlCommand = new SqlCommand())
                    {
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText = ETLLongInProcessDBStageTable;
                        sqlCommand.Parameters.Add(new SqlParameter("@ControlLogId", controlLogId));

                        sqlConnection.Open();
                        SqlDataReader sqlDataReader;
                        using (sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            if (sqlDataReader.HasRows)
                            {
                                while (sqlDataReader.Read())
                                {
                                    try
                                    {
                                        DataRow tableRow = dtETLData.NewRow();
                                        tableRow["TimeTaken"] = sqlDataReader["RunTimeMinutes"];
                                        tableRow["StartTime"] = sqlDataReader["StartTime"];
                                        tableRow["DatabaseName"] = sqlDataReader["DatabaseName"];
                                        tableRow["ServerName"] = sqlDataReader["ServerName"];
                                        tableRow["Subscription_Status"] = sqlDataReader["Sub_Status"];
                                        tableRow["IsRequired"] = sqlDataReader["IsRequired"];
                                        tableRow["Sequence"] = sqlDataReader["SequenceNum"];

                                        dtETLData.Rows.Add(tableRow);
                                    }
                                    catch (Exception e)
                                    {
                                        var logError = new ServiceInvoke();
                                        logError.POST(e);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var logError = new ServiceInvoke();
                logError.POST(e);
            }

            return dtETLData;
        }
    }
}
