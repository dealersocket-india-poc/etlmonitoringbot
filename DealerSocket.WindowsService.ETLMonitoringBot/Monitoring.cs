﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;

namespace DealerSocket.WindowsService.ETLMonitoringBot
{
    public partial class Monitoring : ServiceBase
    {
        Timer timer = new Timer();

        public Monitoring()
        {
            InitializeComponent();
        }

        
        protected override void OnStart(string[] args)
        {
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval =
                Convert.ToDouble(
                    ConfigurationManager.AppSettings["TimerInMiliSeconds"]); //600000; //number in milisecinds  
            timer.Enabled = true;
            
            //isETLComplete = MonitorETL();
        }

        private bool isETLComplete = true;

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            //(new ServiceInvoke()).POST(new Exception("OnElapsedTime"));
            var etlStartTimeConfig = ConfigurationManager.AppSettings["StartTimeMT"];
            var etlStartTime = new TimeSpan(); //"02:00:10";
            TimeSpan.TryParseExact(etlStartTimeConfig, "T", new DateTimeFormatInfo(), TimeSpanStyles.None,
                out etlStartTime);

            var etlInternalInMinutes = Convert.ToInt64(timer.Interval) / (1000 * 60);
            //var etlStartTimeFrame = etlStartTime.Add((TimeSpan.FromMinutes(etlInternalInMinutes)));
            var etlStartTimeFrame = etlStartTime.Add((TimeSpan.FromHours(3)));

            var currentTimeSpan = DateTime.Now.TimeOfDay;

            var serviceTimer = new ServiceInvoke();
            if (etlStartTime < currentTimeSpan && currentTimeSpan <= etlStartTimeFrame)
                //serviceTimer.POST(new Exception("EDR Monitoring Robot Service Running."));
                isETLComplete = false;

            //(new ServiceInvoke()).POST(new Exception("OnElapsedTime: " + isETLComplete.ToString()));

            if (!isETLComplete)
                isETLComplete = !MonitorETL();
            else if (currentTimeSpan.Hours % 6 == 0 && currentTimeSpan.Minutes + etlInternalInMinutes >= 60)
                serviceTimer.POST(new Exception("EDR Monitoring Robot Service Sleeping."));
        }

        protected override void OnStop()
        {
            var serviceTimer = new ServiceInvoke();
            serviceTimer.POST(new Exception("EDR Monitoring Robot Service Stopped."));
        }

        private string lastEmailSentStatus = string.Empty;
        private int timeSpentOnStep = 0;

        private bool MonitorETL()
        {
            var shouldContinue = true;
            try
            {
                //(new ServiceInvoke()).POST(new Exception("Monitoring ETL"));

                var dbContext = new DBUntill();
                var invok = new ServiceInvoke();

                var statusTable = dbContext.GetETLSTatus();

                var etlStatus = "";
                var etlControlLogId = "";
                if (statusTable.Rows.Count > 0)
                {
                    etlStatus = statusTable.Rows[0]["ShortStatus"].ToString();
                    etlControlLogId = statusTable.Rows[0]["ControlLogId"].ToString();

                    //(new ServiceInvoke()).POST(new Exception("ETL Status: " + etlStatus));

                    if (!etlStatus.Equals(lastEmailSentStatus, StringComparison.OrdinalIgnoreCase))
                    {
                        var msgContent = new Content();
                        timeSpentOnStep = 0;
                        
                        if (etlStatus == "COMPL" || etlStatus == "CMPER")
                        {
                            var msgToPost = "";
                            //ETL Status
                            msgContent = new Content(statusTable);
                            msgToPost += msgContent.ConvertDataTableIntoHtml("");

                            //List of high level process with start and end time
                            var summaryTable = dbContext.GetCompletedStepSummary(etlControlLogId);
                            if (summaryTable.Rows.Count > 0)
                            {
                                msgContent = new Content(summaryTable);
                                msgToPost += msgContent.ConvertDataTableIntoHtml("Time taken by each step:");
                            }
                            
                            //DEALER SITE TO STAGING DB Report
                            var top3StageDB = dbContext.GetTopLongRunningDBStageTable(etlControlLogId);
                            if (top3StageDB.Rows.Count > 0)
                            {
                                msgContent = new Content(top3StageDB);
                                msgToPost += msgContent.ConvertDataTableIntoHtml(
                                    "Most time consuming DBs in DEALER SITE TO STAGING");
                            }

                            if (summaryTable.Rows.Count > 0)
                            {
                                //FACT TABLE LOAD
                                var factLoadRow =
                                    summaryTable.Select("StepDiscription = 'FACT TABLE LOAD'");

                                var factProcessDetailId = "";
                                if (factLoadRow.Length > 0)
                                    factProcessDetailId = factLoadRow[0]["ProcessDetailId"].ToString();

                                var topLongRunningSteps =
                                    dbContext.GetETLTopLongRunningSteps(factProcessDetailId);
                                if (topLongRunningSteps.Rows.Count > 0)
                                {
                                    msgContent = new Content(topLongRunningSteps);
                                    msgToPost += msgContent.ConvertDataTableIntoHtml(
                                        "Most time consuming steps in " +
                                        factLoadRow[0]["StepDiscription"]
                                            .ToString());
                                }

                                //PROCESS CUBE KPI
                                var CubeKPIRow =
                                    summaryTable.Select("StepDiscription = 'PROCESS CUBE KPI'");

                                var cubeProcessDetailId = "";
                                if (CubeKPIRow.Length > 0)
                                    cubeProcessDetailId = CubeKPIRow[0]["ProcessDetailId"].ToString();

                                //===================================
                                // NOT REQUIRED AS ITS ONLY ONE STAGE PROCESS
                                //===================================
                                //topLongRunningSteps =
                                //    dbContext.GetETLTopLongRunningSteps(cubeProcessDetailId);
                                //if (topLongRunningSteps.Rows.Count > 0)
                                //{
                                //    msgContent = new Content(topLongRunningSteps);
                                //    msgToPost += msgContent.ConvertDataTableIntoHtml(
                                //        "Most time consuming steps in " +
                                //        CubeKPIRow[0]["StepDiscription"]
                                //            .ToString());
                                //}

                                //Data Volume in Fact
                                var dataVolumeProcessedInFact =
                                    dbContext.GetETLTopLongRunningSteps(factProcessDetailId, true);
                                if (dataVolumeProcessedInFact.Rows.Count > 0)
                                {
                                    msgContent = new Content(dataVolumeProcessedInFact);
                                    msgToPost += msgContent.ConvertDataTableIntoHtml(
                                        "Data Volume in FACT TABLE LOAD with total records: " + string.Format("{0:#,0}",
                                            dbContext.GetETLTotalDataVolume(factProcessDetailId)));
                                }
                            }

                            msgToPost = msgContent.ConvertIntoJson("Summary Report", msgToPost);

                            invok.POST(msgToPost, true);

                            if (etlStatus == "COMPL")
                                shouldContinue = false;
                        }
                        else
                        {
                            //(new ServiceInvoke()).POST(new Exception("ETL Status: Posting "));
                            msgContent = new Content(statusTable);
                            invok.POST(msgContent.ConvertIntoJson(""));
                            //(new ServiceInvoke()).POST(new Exception(msgContent.ConvertIntoJson("")));
                        }
                    }
                    else if (etlStatus == "STGST")
                    {
                        if (timeSpentOnStep % 30 == 0)
                        {
                            var dbCount = dbContext.GetETLStageCompleteDBCount(etlControlLogId);
                            var msgContent = new Content();
                            invok.POST(msgContent.ConvertIntoJson(
                                "DEALER SITE TO STAGING DB Completed Count till now: " + dbCount.ToString()));
                        }
                        if (timeSpentOnStep >= 30)
                        {
                            var longInProcessDB = dbContext.GetLongInProcessDBStageTable(etlControlLogId);

                            if (longInProcessDB.Rows.Count > 0)
                            {
                                var msgContent = new Content(longInProcessDB);
                                invok.POST(msgContent.ConvertIntoJson(
                                    "DEALER SITE TO STAGING - long running DB Process"));
                            }
                        }
                    }
                    else if (etlStatus == "FCTST" && timeSpentOnStep >= 40)
                    {
                        var factLongRunningProcess = dbContext.GetETLTopLongRunningFactProcess(
                            etlControlLogId);

                        if (factLongRunningProcess.Rows.Count > 0)
                        {
                            var msgContent = new Content(factLongRunningProcess);
                            invok.POST(msgContent.ConvertIntoJson(
                                "Running process in FACT TABLE LOAD"));
                        }
                    }

                    lastEmailSentStatus = etlStatus;
                    timeSpentOnStep += Convert.ToInt32(Convert.ToInt64(timer.Interval) / (1000 * 60));
                }
            }
            catch (Exception e)
            {
                var logError = new ServiceInvoke();
                logError.POST(e);
            }

            return shouldContinue;
        }
    }
}
