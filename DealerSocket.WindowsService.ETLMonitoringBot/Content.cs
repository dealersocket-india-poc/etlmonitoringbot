﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealerSocket.WindowsService.ETLMonitoringBot
{
    class Content
    {
        private DataTable dtETL;
        public Content()
        {
            this.dtETL = new DataTable();
        }

        public Content(DataTable etlInformationDataTable)
        {
            this.dtETL = etlInformationDataTable;
        }

        internal string ConvertIntoJson(string bodyTitle)
        {
            return ConvertIntoJsonObj(ConvertDataTableIntoHtml(bodyTitle)).ToString();
        }

        internal string ConvertIntoJson(string bodyTitle, string bodyContent)
        {
            return ConvertIntoJsonObj(bodyContent, bodyTitle).ToString();
        }

        private JObject ConvertIntoJsonObj(string textToPost, string title = "Update")
        {
            JObject obj = new JObject();
            obj.Add(new JProperty("@context", "https://schema.org/extensions"));
            obj.Add(new JProperty("@type", "MessageCard"));
            obj.Add(new JProperty("themeColor", "0072C6"));
            obj.Add(new JProperty("title", "EDR " + title + " for " + DateTime.Now.ToString("dd-MMM-yyyy")));
            obj.Add(new JProperty("text", textToPost));

            return obj;
        }

        internal string ConvertDataTableIntoHtml(string bodyTitle)
        {
            var textToPost = "";

            if (!string.IsNullOrEmpty(bodyTitle))
                textToPost = "<u>" + bodyTitle + "</u></br>";

            if (this.dtETL.TableName.Equals("ETLStatus", StringComparison.OrdinalIgnoreCase))
            {
                textToPost += "<b>ETL Status:</b> " + this.dtETL.Rows[0]["LongStatus"] + " at " +
                              RemoveDateFromText(this.dtETL.Rows[0]["StartTime"].ToString());

            }
            else if (this.dtETL.TableName.Equals("ETLStepSummary", StringComparison.OrdinalIgnoreCase))
            {
                foreach (DataRow row in this.dtETL.Rows)
                {
                    textToPost += "<b>" + row["StepDiscription"].ToString() + "</b> (" + RemoveCompletedText(row["StepSatus"].ToString()) + ") took <b>" + row["TimeTaken"] +
                                  " minute(s)</b> (" +
                                  RemoveDateFromText(row["StartTime"].ToString()) + " to " + RemoveDateFromText(row["EndTime"].ToString()) + ")<br/>";
                }

            }
            else if (this.dtETL.TableName.Equals("ETLTop3LongRunningFact", StringComparison.OrdinalIgnoreCase))
            {
                foreach (DataRow row in this.dtETL.Rows)
                {
                    textToPost += "<b>" + row["ProcessName"].ToString() + "</b> is still running since <b>" + row["TimeTaken"] +
                                  " minute(s)</b> (" +
                                  RemoveDateFromText(row["StartTime"].ToString()) + ") ";

                    textToPost += "</br>";
                }
            }
            else if (this.dtETL.TableName.Equals("ETLTop3LongRunningDBStage", StringComparison.OrdinalIgnoreCase))
            {
                foreach (DataRow row in this.dtETL.Rows)
                {
                    textToPost += "<b>" + row["DatabaseName"].ToString() + "</b> (" + RemoveCompletedText(row["LongStatus"].ToString()) + ") took <b>" + row["TimeTaken"] +
                                  " minute(s)</b> (" +
                                  RemoveDateFromText(row["StartTime"].ToString()) + " to " + RemoveDateFromText(row["EndTime"].ToString()) + ") - " + row["ServerName"].ToString();

                    textToPost += "</br>";
                }
            }
            else if (this.dtETL.TableName.Equals("ETLTop3LongRunning", StringComparison.OrdinalIgnoreCase))
            {
                foreach (DataRow row in this.dtETL.Rows)
                {
                    textToPost += "<b>" + row["ProcessName"].ToString() + "</b> took <b>" + row["TimeTaken"] +
                                  " minute(s)</b> (" +
                                  RemoveDateFromText(row["StartTime"].ToString()) + " to " + RemoveDateFromText(row["EndTime"].ToString()) + ") ";

                    var totalRecords = Convert.ToInt64(row["RecordsInserted"]) + Convert.ToInt64(row["RecordsDeleted"]) +
                                       Convert.ToInt64(row["RecordsUpdated"]);

                    if (totalRecords > 0)
                        textToPost += "<b>Total Records:</b> " + string.Format("{0:#,0}", totalRecords);

                    textToPost += "</br>";
                }
            }
            else if (this.dtETL.TableName.Equals("ETLLongInProcessDBStage", StringComparison.OrdinalIgnoreCase))
            {
                foreach (DataRow row in this.dtETL.Rows)
                {
                    textToPost += "<b>" + row["DatabaseName"].ToString() + "</b> (<font color='#1d86b8'><b>" + row["Subscription_Status"].ToString() + "</b></font> - " + row["IsRequired"].ToString() + ") is still running since <b>" + row["TimeTaken"] +
                                  " minute(s)</b> with <b>" + row["Sequence"] + "</b> (started at " +
                                  RemoveDateFromText(row["StartTime"].ToString()) + " on <font color='#1d86b8'>" + row["ServerName"].ToString() + "</font>) ";

                    textToPost += "</br>";
                }
            }

            textToPost += "</br></br>";

            return textToPost;
        }

        internal string RemoveDateFromText(string dateTimeText)
        {
            return dateTimeText.Replace(DateTime.Now.ToString("M/d/yyyy") + " ", "");
        }

        internal string RemoveCompletedText(string status)
        {
            return status.Replace("COMPLETE - ", "");
        }
    }
}
