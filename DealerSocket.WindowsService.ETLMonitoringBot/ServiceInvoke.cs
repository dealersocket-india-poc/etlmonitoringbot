﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DealerSocket.WindowsService.ETLMonitoringBot
{
    class ServiceInvoke
    {
        internal void POST(string jsonContent, bool isSummaryReport = false)
        {
            string url = ConfigurationManager.AppSettings["EDRProsessingTeamChannelPostHook"];

            if(isSummaryReport)
                url = ConfigurationManager.AppSettings["EDRReportSummaryPostHook"];

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }
            long length = 0;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    length = response.ContentLength;
                }
            }
            catch (WebException ex)
            {
                POST(ex);
            }
        }

        internal void POST(Exception exp)
        {
            string url = ConfigurationManager.AppSettings["ErrorTeamChannelPostHook"];

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";

            string textToPost = exp.Message.ToString();

            JObject obj = new JObject();
            obj.Add(new JProperty("@context", "https://schema.org/extensions"));
            obj.Add(new JProperty("@type", "MessageCard"));
            obj.Add(new JProperty("themeColor", "0072C6"));
            obj.Add(new JProperty("title", "EDR Robot Service " + DateTime.Now.ToString("dd-MMM-yyyy") + " - Machine: " + Environment.MachineName));
            obj.Add(new JProperty("text", textToPost));

            string jsonContent = obj.ToString();

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }
            long length = 0;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    length = response.ContentLength;
                }
            }
            catch (WebException ex)
            {
                //LogError
            }
        }
    }
}
